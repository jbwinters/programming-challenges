from sys import stdin

def get_largest_decent(k):
    # split k into two parts: n ++ m
    # n is divisible by 3
    # m is divisible by 5

    n = k
    m = 0

    while n > 0:
        n = n - (n % 3)
        m = k - n
        if m % 5 == 0:
            break
        n -= 1

    decent_string = '5' * n + '3' * m
    if n + m != k:
        return '-1'

    return decent_string


def main():
    var = int(stdin.readline()) # says how many tests there are
    for line in stdin:
        print(get_largest_decent(int(line)))

if __name__ == '__main__':
    main()

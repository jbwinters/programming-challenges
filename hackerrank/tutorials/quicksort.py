def partition(ar):
    if len(ar) <= 1:
        return ar
    pivot = ar.pop(0)
    less = []
    greater = []
    for v in ar:
        if v < pivot:
            less.append(v)
        if v > pivot:
            greater.append(v)
    ret = partition(less) + [pivot] + partition(greater)
    print ' '.join(str(x) for x in ret)
    return ret

m = input()
ar = [int(i) for i in raw_input().strip().split()]
partition(ar)

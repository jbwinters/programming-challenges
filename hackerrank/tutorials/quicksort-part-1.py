def partition(ar, pivot):
    less_than_pivot = []
    greater_than_pivot = []
    for v in ar:
        if v < pivot:
            less_than_pivot.append(v)
        if v > pivot:
            greater_than_pivot.append(v)
    return less_than_pivot + [v] + greater_than_pivot

m = input()
ar = [int(i) for i in raw_input().strip().split()]
sorted_ar = partition(ar[1:], ar[0])
print ' '.join(str(x) for x in sorted_ar)

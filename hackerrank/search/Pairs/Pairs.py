from sys import stdin

def main():
    N, K = (int(x) for x in stdin.next().split())
    array = set((int(x) for x in stdin.next().split()))
    pair_k_count = 0
    for elt in array:
        if elt + K in array:
            pair_k_count += 1

    print pair_k_count


if __name__ == '__main__':
    main()

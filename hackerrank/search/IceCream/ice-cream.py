from sys import stdin

def find_flavors(m, n, C):
    for i in xrange(n):
        for j in xrange(i + 1, n):
            if C[i] + C[j] == m:
                return i + 1, j + 1
    
    return None, None

def main():
    lines = [line.strip() for line in stdin.readlines()]
    T = int(lines[0])

    next_case = 1
    for i in xrange(T):
        m = int(lines[next_case])
        n = int(lines[next_case + 1])
        C = [int(c) for c in lines[next_case + 2].split()]

        next_case = next_case + 3
        f1, f2 = find_flavors(m, n, C)
        if f1 and f2:
            print f1, f2
        else: # each test case supposedly has a solution so this shouldn't happen
            print 'None'

if __name__ == '__main__':
    main()

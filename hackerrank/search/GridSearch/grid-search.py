from sys import stdin

def is_match(search_space, i, j, query):
    search_rows = len(search_space)
    search_cols = len(search_space[0])
    query_rows = len(query)
    query_cols = len(query[0])
    if i + query_rows > search_rows:
        return False
    if j + query_cols > search_cols:
        return False

    for qi in xrange(query_rows):
        for qj in xrange(query_cols):
            if query[qi][qj] != search_space[i + qi][j + qj]:
                return False
    return True

def find(query, space, space_rows, space_cols):
    for i in xrange(space_rows):
        for j in xrange(space_cols):
            if space[i][j] == query[0][0]:
                if is_match(space, i, j, query):
                    return "YES"

    return "NO"

def main():
    lines = stdin.readlines()
    T = int(lines[0])
    cur_line = 1
    for test_case in xrange(T):
        search_space_dim = [int(d) for d in lines[cur_line].split()]
        cur_line += 1
        search_space = []
        for i in xrange(search_space_dim[0]):
            search_space.append([int(c) for c in lines[cur_line].strip()])
            cur_line += 1

        query_dim = [int(d) for d in lines[cur_line].split()]
        cur_line += 1

        query = []
        for i in xrange(query_dim[0]):
            query.append([int(c) for c in lines[cur_line].strip()])
            cur_line += 1
        print find(query, search_space, search_space_dim[0], search_space_dim[1])


if __name__ == '__main__':
    main()

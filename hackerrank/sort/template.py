from sys import stdin

def main():
    var = int(stdin.next())

    arr = [int(x) for x in stdin.next().split()]
    print var, arr

if __name__ == '__main__':
    main()

from sys import stdin

# check that a value is in the right spot
def is_valid(arr, i):
    below = float('-inf')
    above = float('inf')
    if i > 0:
        below = arr[i - 1]
    if i < len(arr) - 1:
        above = arr[i + 1]

    return below <= arr[i] <= above

def can_swap(arr, i1, i2):
    dup = list(arr)
    tmp = arr[i1]
    dup[i1] = arr[i2]
    dup[i2] = arr[i1]
    return is_valid(dup, i1) and is_valid(dup, i2)

def main():
    var = int(stdin.next())

    arr = [int(x) for x in stdin.next().split()]

    bad = []

    should_be = sorted(arr)
    for i in xrange(len(arr)):
        if arr[i] != should_be[i]:
            bad.append(i)

    if len(bad) == 0:
        print 'yes'
    elif len(bad) == 1:
        print 'no'
    elif len(bad) == 2:
        if can_swap(arr, bad[0], bad[1]):
            print 'yes'
            print 'swap', bad[0] + 1, bad[1] + 1
        else:
            print 'no'
    else: # check for reverse
        # contiguous block
        max_bad_i = max(bad)
        min_bad_i = min(bad)
        contiguous = True

        # if diff is > len definitely no
        if max_bad_i - min_bad_i > len(bad):
            print 'no'
        else:
            to_check = [arr[i] for i in bad] # don't worry about middle

            if sorted(to_check, reverse=True) != to_check:
                print 'no'
            else:
                print 'yes'
                print 'reverse', min_bad_i + 1, max_bad_i + 1



if __name__ == '__main__':
    main()

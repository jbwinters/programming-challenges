import sys
import heapq
from collections import deque

class Record(object):
    def __init__(self, record):
        self.id = record[0]
        self.hep = tuple(int(v.split(':')[1]) for v in record[1:])

class Circuit(Record):
    def __init__(self, record, max_jugglers):
        super(Circuit, self).__init__(record)
        self.jugglers = []
        self.max_jugglers = max_jugglers

    def is_full(self):
        return len(self.jugglers) == self.max_jugglers

    def insert_if_possible(self, juggler):
        match = juggler.match(self.id)
        if not match:
            match = match_score(juggler.hep, self.hep)
        unplaced_juggler = (match, juggler)
        if not self.is_full():
            heapq.heappush(self.jugglers, unplaced_juggler)
            unplaced_juggler = (0, None)
        elif unplaced_juggler > heapq.nsmallest(1, self.jugglers):
            unplaced_juggler = heapq.heapreplace(self.jugglers, unplaced_juggler)
        return unplaced_juggler[1]

    def juggler_preferences(self):
        juggler_prefs = ((j[1].id, j[1].preferences_string())
            for j in self.jugglers)
        return juggler_prefs

class Juggler(Record):
    def __init__(self, record, circuits):
        super(Juggler, self).__init__(record[:-1])
        self.preferred_circuits = record[-1].split(',') # for output at the end
        self.preferences_queue = deque(self.preferred_circuits)
        self.circuit_matches = {}
        for circuit in self.preferred_circuits:
            self.circuit_matches[circuit] = match_score(self.hep,
                    circuits[circuit].hep)

    def match(self, c_id):
        try:
            return self.circuit_matches[c_id]
        except KeyError:
            return None

    def next_preference(self):
        return self.preferences_queue.popleft()

    def preferences_string(self):
        return ' '.join(c_id + ':' + str(self.circuit_matches[c_id])
                for c_id in self.preferred_circuits)

def match_score(juggler_hep, circuit_hep):
    return sum(a * b for a, b in zip(juggler_hep, circuit_hep))

def juggler_circuits(filename):
    # Create circuit and juggler instances from the file input
    juggler_lines, circuit_lines = read_file(filename)
    assert((len(juggler_lines) % len(circuit_lines)) == 0)

    jugglers_per_circuit = len(juggler_lines) / len(circuit_lines)
    jugglers = deque()
    circuits = {}
    for line in circuit_lines:
        circuits[line[0]] = Circuit(line, jugglers_per_circuit)
    for line in juggler_lines:
        jugglers.append(Juggler(line, circuits))

    # Place jugglers according to their preferred circuits
    # If they are replaced by another juggler or can't be
    # placed in their next preferred circuit, return them
    # to the juggler queue.
    leftover_jugglers = deque()
    while jugglers:
        juggler = jugglers.popleft()
        try:
            preferred_circuit = juggler.next_preference()
        except IndexError:
            leftover_jugglers.append(juggler)
            continue
        poor_fit_juggler = circuits[preferred_circuit].insert_if_possible(juggler)
        if poor_fit_juggler:
            jugglers.append(poor_fit_juggler)
    print len(leftover_jugglers)

    # Place leftover jugglers (who didn't get into any of their preferences)
    # None of them can switch into a circuit they prefer more,
    # so it doesn't matter where they are placed.
    for c_id in circuits:
        while not circuits[c_id].is_full():
            circuits[c_id].insert_if_possible(leftover_jugglers.popleft())
    return circuits

def read_file(filename):
    jugglers = []
    circuits = []
    with open(filename) as f:
        for line in f:
            line = line.split()
            if not line:
                continue
            if line[0] == 'C':
                circuits.append(line[1:])
            elif line[0] == 'J':
                jugglers.append(line[1:])
    return jugglers, circuits

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print 'Use: python juggle-fest.py <filename>'
        exit(0)
    if sys.argv[1] == 'test':
        import doctest
        doctest.testmod()
        exit(0)
    filename = sys.argv[1]
    circuits = juggler_circuits(filename)

    for c_id in circuits:
        print c_id, ' '.join(j_id + ' ' + j_prefs
            for j_id, j_prefs in circuits[c_id].juggler_preferences())

    # for submission
    c_id = 'C1790'
    if c_id in circuits:
        print c_id, ' '.join(j_id + ' ' + j_prefs
            for j_id, j_prefs in circuits[c_id].juggler_preferences())
        print sum(int(j[1].id.strip('J')) for j in circuits[c_id].jugglers)

import sys
import numpy as np

def juggler_circuits(filename):
    juggler_lines, circuit_lines = read_file(filename)
    jugglers = []
    circuits = []
    for line in circuit_lines:
        hep = np.array([int(v.split(':')[1]) for v in line[1:]])
        circuits.append(hep)
    for line in juggler_lines:
        hep = np.array([int(v.split(':')[1]) for v in line[1:4]])
        jugglers.append(hep)


    for x, c in enumerate(circuits):
        print x, ':',
        for y, j in enumerate(jugglers):
            print np.dot(j, c),
        print

def read_file(filename):
    jugglers = []
    circuits = []
    with open(filename) as f:
        for line in f:
            line = line.split()
            if not line:
                continue
            if line[0] == 'C':
                circuits.append(line[1:])
            elif line[0] == 'J':
                jugglers.append(line[1:])
    return jugglers, circuits

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print 'Use: python juggle-fest.py <filename>'
        exit(0)
    if sys.argv[1] == 'test':
        import doctest
        doctest.testmod()
        exit(0)
    filename = sys.argv[1]
    juggler_circuits(filename)



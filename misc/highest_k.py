import sys

def find_k_highest(array, k=4):
    quickfind_top_k(array, 0, len(array) - 1, k)
    return array[-k:]

def quickfind_top_k(array, left, right, k):
    if right > left:
        pivot_index = left + (right - left) / 2
        new_pivot_index = partition(array, left, right, pivot_index)
        if new_pivot_index > right - k + 1:
            quickfind_top_k(array, left, new_pivot_index - 1,
                    new_pivot_index - (right-k + 1))
        if new_pivot_index < right - k + 1:
            quickfind_top_k(array, new_pivot_index + 1, right, k)

def partition(array, left, right, pivot_index):
    pivot = array[pivot_index]
    swap(array, pivot_index, right)
    store_index = left
    for i in xrange(left, right):
        if array[i] < pivot:
            swap(array, store_index, i)
            store_index += 1
    swap(array, right, store_index)
    return store_index

def swap(array, index1, index2):
    array[index1], array[index2] = array[index2], array[index1]

def find_k_highest_with_overhead(array, k=4):
    if len(array) <= k:
        return array
    pivot = array.pop(len(array) / 2)
    greater = []
    less = []
    for v in array:
        if v >= pivot:
            greater.append(v)
        else:
            less.append(v)
    highest = find_k_highest(greater, k=k)
    if len(highest) < k:
        highest.append(pivot)
    next_highest = [] if len(highest) >= k \
            else find_k_highest(less, k=k-len(highest))
    return highest + next_highest

if __name__ == '__main__':
    array = [7,32,7,2,1,5,3,78,-23,9,0]
    # 78, 32, 9, 7
    k_in = 4
    try:
        k_in = int(sys.argv[1])
    except:
        pass
    print ' '.join(str(x) for x in find_k_highest(array, k=k_in))

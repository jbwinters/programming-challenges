import sys

def max_total_of_triangle(filename):
    '''
    >>> max_total_of_triangle('test_inputs/triangle00.txt')
    27
    >>> max_total_of_triangle('test_inputs/triangle01.txt')
    732506
    >>> max_total_of_triangle('test_inputs/triangle02.txt')
    23
    >>> max_total_of_triangle('test_inputs/triangle03.txt')
    1074
    '''
    triangle = []
    with open(filename) as f:
        for line in f:
            triangle.append([int(x) for x in line.split()])
    prev_row = triangle[-1]
    for row in reversed(triangle[0:-1]):
        for i, value in enumerate(row):
            row[i] += max(prev_row[i], prev_row[i+1])
        prev_row = row
    return triangle[0][0]

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print 'Use: python triangle-max-total.py <filename>'
        exit(0)
    if sys.argv[1] == 'test':
        import doctest
        doctest.testmod()
        exit(0)
    filename = sys.argv[1]
    print max_total_of_triangle(filename)

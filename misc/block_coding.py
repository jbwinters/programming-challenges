"""
Solution to the block coding puzzle:
    Given a height and width, determine the number of
    possible ways to build a wall where no two walls
    in adjacent rows have any overlapping ends.
Generalized to work for any block set
"""
import sys
import itertools
from collections import defaultdict
from fractions import gcd

def row_permutations(width, block_sizes):
    r'''
    Create a list of all possible permutations of rows with a given width.
    'Rows' are frozensets of all gaps between two blocks in each permutation.

    >>> block_sizes = (3.0, 4.5)
    >>> rows = row_permutations(12.0, block_sizes)
    >>> print "\n".join((str(row) for row in rows))
    frozenset([9.0, 3.0, 6.0])
    frozenset([7.5, 3.0])
    frozenset([4.5, 7.5])
    frozenset([4.5, 9.0])
    '''
    if width < min(block_sizes) or width % reduce(gcd, block_sizes):
        # Any width not divisible by by the greatest common denominator of
        # the block sizes will have no solutions.
        # Any width smaller than the smallest block will have no solutions.
        return []
    return build_permutations_list(width, block_sizes)

def build_permutations_list(width, block_sizes, row=None, row_total=0):
    if not row:
        row = []
    rows = []
    # Build all rows that complete the row built so far
    for block in block_sizes:
        new_size = row_total + block
        if new_size == width:
            # Rows only include gaps between two blocks, not ends,
            # so stop recursing before placing last block in each row
            rows.append(frozenset(row))
            continue
        elif new_size > width:
            # There isn't enough remaining space to place this block in this row
            continue
        # recurse to find all permutations
        rows += build_permutations_list(width, block_sizes,
                row + [new_size], new_size)
    return rows

def next_row_mappings(rows):
    r'''
    Map each row to a list of rows that it can be adjacent to.

    >>> rows = [frozenset([9.0, 3.0, 6.0]), frozenset([7.5, 3.0]),\
    ... frozenset([4.5, 7.5]), frozenset([4.5, 9.0])]
    >>> d = next_row_mappings(rows)
    >>> print '\n'.join(str(d[key]) for key in d)
    [frozenset([4.5, 7.5])]
    [frozenset([4.5, 9.0])]
    [frozenset([9.0, 3.0, 6.0])]
    [frozenset([7.5, 3.0])]
    '''
    next_rows = defaultdict(list)
    for row, next_row in itertools.permutations(rows, 2):
        # Rows that can be adjacent will have no elements in common.
        if row.isdisjoint(next_row):
            next_rows[row].append(next_row)
    if (frozenset() in next_rows) or (len(rows) == 1 and frozenset([]) in rows):
        # Special case for empty set (meaning one block fills the wall).
        # In the case where only one block size fits,
        # make sure it maps to itself since it wont
        # be included by itertools.permutations()
        next_rows[frozenset()].append(frozenset())
    return next_rows

def number_of_solutions(width, height, block_sizes):
    '''
    Dynamic programming algorithm to find the number of ways to build a wall
    where no gaps between blocks line up:
        Build a table with $height rows where each row
        maps each permutation to the number of times
        it could possibly be used in that row by all
        possible solutions. The number of possible
        solutions for that height and width is the
        sum of the last row of the table.

    >>> block_sizes = (3.0, 4.5)
    >>> number_of_solutions(3, 2, block_sizes)
    1
    >>> number_of_solutions(7.5, 1, block_sizes)
    2
    >>> number_of_solutions(7.5, 2, block_sizes)
    2
    >>> number_of_solutions(12, 3, block_sizes)
    4
    >>> number_of_solutions(27, 5, block_sizes)
    7958
    >>> number_of_solutions(48, 10, block_sizes)
    806844323190414L
    >>> block_sizes_2 = (1.0, 2.0, 3.0, 4.0)
    >>> number_of_solutions(2, 2, block_sizes_2)
    3
    >>> number_of_solutions(3, 2, block_sizes_2)
    9
    '''
    # Create list of permutations
    rows = row_permutations(width, block_sizes)
    if height == 1:
        return len(rows)

    # Map permutations to adjacent permutations
    next_rows = next_row_mappings(rows)

    # Build the solutions table -
    # First row can use all possible permutations
    prev_times_each_row_used = {}
    for key in rows:
        prev_times_each_row_used[key] = 1

    # Remaining rows are dependent on the mapping between permutations
    for i in xrange(height - 1):
        times_each_row_used = defaultdict(int)
        for key in prev_times_each_row_used:
            for row in next_rows[key]:
                times_each_row_used[row] += prev_times_each_row_used[key]
        prev_times_each_row_used = times_each_row_used

    # Number of solutions is the sum of the last row of the table
    return sum(times_each_row_used.values())

if __name__ == '__main__':
    if len(sys.argv) == 2 and sys.argv[1] == "test":
        # 'python block_coding.py test' will run the tests for this code
        import doctest
        doctest.testmod()
        exit(0)
    elif len(sys.argv) != 3:
        print "Use: python block_coding.py <width> <height>"
        print " or: python block_coding.py test"
        exit(0)

    # format command line arguments
    width = float(sys.argv[1])
    height = int(sys.argv[2])

    # find the number of solutions for the provided height, width, and blocks
    block_sizes = (3.0, 4.5)
    print number_of_solutions(width, height, block_sizes)

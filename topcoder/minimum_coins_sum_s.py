"""http://www.topcoder.com/tc?d1=tutorials&d2=dynProg&module=Static"""
import sys
import numpy as np

def main(S):
    coins = np.array([int(c) for c in sys.stdin.readline().split()])
    mins = [0] + [float('inf')]*S
    for i in range(1, S + 1):
        j = 0
        for coin in coins[coins[j::] <= i]:
            #print i, i-coin, len(mins)
            #print mins[i]
            if mins[i - coin] + 1 < mins[i]:
                mins[i] = mins[i - coin] + 1
            j += 1

    print mins[S]
    print mins


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print "Arguments: <S>"
        exit(0)
    s = int(sys.argv[1])
    main(s)
